# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the partman-auto package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: partman-auto\n"
"Report-Msgid-Bugs-To: partman-auto@packages.debian.org\n"
"POT-Creation-Date: 2024-11-26 20:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid "Help on partitioning"
msgstr ""

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Partitioning a hard drive consists of dividing it to create the space needed "
"to install your new system.  You need to choose which partition(s) will be "
"used for the installation."
msgstr ""

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid "Select a free space to create partitions in it."
msgstr ""

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Select a device to remove all partitions in it and create a new empty "
"partition table."
msgstr ""

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Select a partition to remove it or to specify how it should be used. At a "
"bare minimum, you need one partition to contain the root of the file system "
"(whose mount point is /).  Most people also feel that a separate swap "
"partition is a necessity.  \"Swap\" is scratch space for an operating "
"system, which allows the system to use disk storage as \"virtual memory\"."
msgstr ""

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"When the partition is already formatted you may choose to keep and use the "
"existing data in the partition.  Partitions that will be used in this way "
"are marked with \"${KEEP}\" in the main partitioning menu."
msgstr ""

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"In general you will want to format the partition with a newly created file "
"system.  NOTE: all data in the partition will be irreversibly deleted.  If "
"you decide to format a partition that is already formatted, it will be "
"marked with \"${DESTROY}\" in the main partitioning menu.  Otherwise it will "
"be marked with \"${FORMAT}\"."
msgstr ""

#. Type: text
#. Description
#. :sl1:
#: ../partman-auto.templates:2001
msgid "Please wait..."
msgstr ""

#. Type: text
#. Description
#. :sl1:
#: ../partman-auto.templates:3001
msgid "Computing the new partitions..."
msgstr ""

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:4001 ../partman-auto.templates:5001
msgid "Failed to partition the selected disk"
msgstr ""

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:4001
msgid ""
"This probably happened because the selected disk or free space is too small "
"to be automatically partitioned."
msgstr ""

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:5001
msgid ""
"This probably happened because there are too many (primary) partitions in "
"the partition table."
msgstr ""

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001 ../partman-auto.templates:10001
msgid "Partitioning method:"
msgstr ""

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001
msgid ""
"The installer can guide you through partitioning a disk (using different "
"standard schemes) or, if you prefer, you can do it manually. With guided "
"partitioning you will still have a chance later to review and customise the "
"results."
msgstr ""

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001 ../partman-auto.templates:10001
msgid ""
"If you choose guided partitioning for an entire disk, you will next be asked "
"which disk should be used."
msgstr ""

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:11001
msgid "Partitioning scheme:"
msgstr ""

#. Type: select
#. Description
#. :sl1:
#. "Selected for partitioning" can be either an entire disk
#. of "the largest continuous free space" on an existing disk
#. TRANSLATORS, please take care to choose something appropriate for both
#.
#. It is followed by a variable giving the chosen disk, hence the colon
#. at the end of the sentence. Please keep it.
#: ../partman-auto.templates:11001
msgid "Selected for partitioning:"
msgstr ""

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:11001
msgid ""
"The disk can be partitioned using one of several different schemes. If you "
"are unsure, choose the first one."
msgstr ""

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:12001
msgid "Unusable free space"
msgstr ""

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:12001
msgid ""
"Partitioning failed because the chosen free space may not be used. There are "
"probably too many (primary) partitions in the partition table."
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:15001
msgid "Guided partitioning"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:16001
msgid "Guided - use the largest continuous free space"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:17001
msgid "Guided - use entire disk"
msgstr ""

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:18001
msgid "Select disk to partition:"
msgstr ""

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:18001
msgid ""
"Note that all data on the disk you select will be erased, but not before you "
"have confirmed that you really want to make the changes."
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#. This is a method for partitioning - as in guided (automatic) versus manual
#: ../partman-auto.templates:19001
msgid "Manual"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:20001
msgid "Automatically partition the free space"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:21001
msgid "All files in one partition (recommended for new users)"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:22001
msgid "Separate /home partition"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:23001
msgid "Separate /home, /var, and /tmp partitions"
msgstr ""

#. Type: text
#. Description
#. :sl2:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:24001
msgid "Small-disk (< 10GB) partitioning scheme"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:25001
msgid "Separate /var and /srv, swap < 1GB (for servers)"
msgstr ""
