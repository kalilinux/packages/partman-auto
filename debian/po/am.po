# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Amharic translation for debian-installer
# This file is distributed under the same license as the debian-installer package.
# tegegne tefera <tefera@mekuria.com>, 2006.
#
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@debian.org>, 2004.
#   Data taken from ICU-2.8; contributed by:
#   - Daniel Yacob <yacob@geez.org>, Ge'ez Frontier Foundation
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-auto@packages.debian.org\n"
"POT-Creation-Date: 2024-11-26 20:02+0000\n"
"PO-Revision-Date: 2022-12-11 11:40+0000\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Amharic <linux-ethiopia@googlegroups.com>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid "Help on partitioning"
msgstr "የመክፈል መመሪያ"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Partitioning a hard drive consists of dividing it to create the space needed "
"to install your new system.  You need to choose which partition(s) will be "
"used for the installation."
msgstr ""
"ዲስክን ከፋፍሎ በቂ ባዶ ቦታ በማዘጋጀት አዲስ ስርዓትን መትከል ይቻላል። ለመትከል የትኞቹ ክፋይ(ዮች)ን መጠቀም "
"እንዳለብን መምረጥ ይኖርብዎታል።"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid "Select a free space to create partitions in it."
msgstr "ክፍዮችን ለመፍጠር ባዶ ቦታን ይምረጡ"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Select a device to remove all partitions in it and create a new empty "
"partition table."
msgstr "ያሉትን ክፋዮች ሁሉ ለመሰረዝና አዲስ ለመፍጠር አካሉን ይምረጡ።"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Select a partition to remove it or to specify how it should be used. At a "
"bare minimum, you need one partition to contain the root of the file system "
"(whose mount point is /).  Most people also feel that a separate swap "
"partition is a necessity.  \"Swap\" is scratch space for an operating "
"system, which allows the system to use disk storage as \"virtual memory\"."
msgstr ""
"ለመሰረዝ ወይም እንዴት በጥቅም ላይ እንደሚውል ለመግለጽ ክፋይን ይምረጡ። ቢያንስ ቢያንስ አንድ ክፋይ የፋይል ስርዓቱን "
"ስር የሚይዝ (መጫኛ ጣቢያው / የሆነ) ያስፈልጋል። አብዛኛው ሰው ራሱን የቻለ የመለወጫ ክፋይ እንዲኖር ይፈልጋሉም። "
"\"Swap\" ወይም መለወጫ የገዥ ስርዓቱ ዲስኩን እንደ \"virtual memory\" እንዲጠቀም ይረዳል።"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"When the partition is already formatted you may choose to keep and use the "
"existing data in the partition.  Partitions that will be used in this way "
"are marked with \"${KEEP}\" in the main partitioning menu."
msgstr ""
"ክፋዩ ቀድሞ የተሟሸ ከሆነ በላዩ ላይ ያለው ዴታ እንዳለ እንዲሆን መምረጥ ይችላሉ። በዚህ ሁኔታ ያሉ ክፋዮች በዋናው "
"የክፋይ ሰንጠረዥ ምናሌ የ\"${KEEP}\"  ምልክት ይደረግባቸዋል።"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"In general you will want to format the partition with a newly created file "
"system.  NOTE: all data in the partition will be irreversibly deleted.  If "
"you decide to format a partition that is already formatted, it will be "
"marked with \"${DESTROY}\" in the main partitioning menu.  Otherwise it will "
"be marked with \"${FORMAT}\"."
msgstr ""
"በተለምዶ ክፋይ በአዲስ የፋይል ስርዓት ይሟሻል። ማስጠንቀቂያ፦ ነክፋዩ ላይ ያለ ዴታ ሁሉ ለመለስ በማይችል ሁኔታ "
"ይሰረዛል። ቀደም ሲል የተሟሸ ክፋይ እንደገና ለማሟሸት ካሰቡ ይህ በዋናው ክፋይ ምናሌ  \"${DESTROY}\" የሚል "
"ምልክት ይሰጠዋል። ይህ ካልሆነ \"${FORMAT}\" የሚል ምልክት ይኖረዋል።"

#. Type: text
#. Description
#. :sl1:
#: ../partman-auto.templates:2001
msgid "Please wait..."
msgstr "እባክዎን ይጠብቁ…"

#. Type: text
#. Description
#. :sl1:
#: ../partman-auto.templates:3001
msgid "Computing the new partitions..."
msgstr "አዲሱን ክፋይ በማስላት ላይ…"

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:4001 ../partman-auto.templates:5001
msgid "Failed to partition the selected disk"
msgstr "የተመረጠውን ዲስክ መክፈል አልተሳካም።"

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:4001
msgid ""
"This probably happened because the selected disk or free space is too small "
"to be automatically partitioned."
msgstr "ይህ የሆነው ምናልባት የተመረጠው ባዶ ቦታ ወይም ዲስክ በራስሰር ለመከፈል ትንሽ ስለሆነ ይሆናል።"

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:5001
msgid ""
"This probably happened because there are too many (primary) partitions in "
"the partition table."
msgstr "ይህ የሆነው ምናልባት ከመጠን ብዙ ዋና ክፋዮች በክፋይ ሰንጠረዡ ላይ ስላሉ ይሆናል።"

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001 ../partman-auto.templates:10001
msgid "Partitioning method:"
msgstr "የከፈላ ዘዴ፦"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001
msgid ""
"The installer can guide you through partitioning a disk (using different "
"standard schemes) or, if you prefer, you can do it manually. With guided "
"partitioning you will still have a chance later to review and customise the "
"results."
msgstr ""
"ተካዩ (የተለያዩ አሰራሮችን በመጠቀም) ዲስክ ለመክፈል ሊመራዎት ይችላል፣ ወይም ከፈለጉ በራስዎ ለማድረግ ይችላሉ። "
"በመመራት ብከፍሉም በኋላ ተመልሰው የተሰራውን ለመቆጣጠርና ለማስተካከል እድል አለዎት።"

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001 ../partman-auto.templates:10001
msgid ""
"If you choose guided partitioning for an entire disk, you will next be asked "
"which disk should be used."
msgstr "ሙሉ ዲስኩን እየተመሩ ለመክፈል ከመረጥ ቀጥሎ የትኛውን ዲስክ መጠቀም እንዳለብን ይጠየቃሉ፡፡"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:11001
msgid "Partitioning scheme:"
msgstr "የመክፈል መርሃግብር፦"

#. Type: select
#. Description
#. :sl1:
#. "Selected for partitioning" can be either an entire disk
#. of "the largest continuous free space" on an existing disk
#. TRANSLATORS, please take care to choose something appropriate for both
#.
#. It is followed by a variable giving the chosen disk, hence the colon
#. at the end of the sentence. Please keep it.
#: ../partman-auto.templates:11001
msgid "Selected for partitioning:"
msgstr "ለመክፈል የተመረጠ፦"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:11001
msgid ""
"The disk can be partitioned using one of several different schemes. If you "
"are unsure, choose the first one."
msgstr "ይህ ዲስክ የተለያየ መርሃግብር በመጠቀም ሊከፈል ይችላል። እርግጠኛ ካልሆኑ የመጀመሪያውን ይምረጡ።"

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:12001
msgid "Unusable free space"
msgstr "የማይሰራ ባዶ ቦታ"

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:12001
msgid ""
"Partitioning failed because the chosen free space may not be used. There are "
"probably too many (primary) partitions in the partition table."
msgstr ""
"የተመረጠውን ባዶ ስፍራ መጠቀም ባለማቻሉ መክፈሉ አልተሳካም። ማንልባት የዋና ክፋዮች ቁጥር ከሚፈቀደው በላይ ይሆናል።"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:15001
msgid "Guided partitioning"
msgstr "ከፈላ በመሪ"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:16001
msgid "Guided - use the largest continuous free space"
msgstr "በመሪ - ትልቁን ተከታታይ ባዶ ቦታ ይጠቀሙ"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:17001
msgid "Guided - use entire disk"
msgstr "በመሪ - ዲስኩን በሙሉ ይጠቀሙ"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:18001
msgid "Select disk to partition:"
msgstr "ዲስክ ለመክፈል ምረጥ፦"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:18001
msgid ""
"Note that all data on the disk you select will be erased, but not before you "
"have confirmed that you really want to make the changes."
msgstr ""
"በዲስኩ ላይ ያለ መረጃ ሁሉ እንደሚሰረዝ ያስተውሉ፡፡ ቢሆንም ለውጡን ማድረግ ይፈልጉ እንደሆን ካረጋገጡ በኋላ ነው፡፡"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#. This is a method for partitioning - as in guided (automatic) versus manual
#: ../partman-auto.templates:19001
msgid "Manual"
msgstr "በእጅ ክፈል"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:20001
msgid "Automatically partition the free space"
msgstr "ባዶ ቦታን በራስሰር ክፈል"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:21001
msgid "All files in one partition (recommended for new users)"
msgstr "ሁሉም ፍይሎች በአንድ ክፋይ (ለአዲስ ተጠቃሚዎች ይህንን ይመክሯል)"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:22001
msgid "Separate /home partition"
msgstr "የ /home ክፋይ ይለይ፦"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:23001
msgid "Separate /home, /var, and /tmp partitions"
msgstr "የተለዩ  /home, /var, and /tmp ክፋዮች"

#. Type: text
#. Description
#. :sl2:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:24001
#, fuzzy
msgid "Small-disk (< 10GB) partitioning scheme"
msgstr "ትንሽ ዲስክ (< 1ጊባ) ክፍያ መርሃግብር"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:25001
msgid "Separate /var and /srv, swap < 1GB (for servers)"
msgstr ""
