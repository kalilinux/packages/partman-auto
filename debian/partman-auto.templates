Template: partman-auto/help
Type: note
# :sl1:
#flag:translate!:8
_Description: Help on partitioning
 Partitioning a hard drive consists of dividing it to create the space
 needed to install your new system.  You need to choose which
 partition(s) will be used for the installation.
 .
 Select a free space to create partitions in it.
 .
 Select a device to remove all partitions in it and create a new empty
 partition table.
 .
 Select a partition to remove it or to specify how it should be used.
 At a bare minimum, you need one partition to contain the root of the
 file system (whose mount point is /).  Most people also feel that a
 separate swap partition is a necessity.  "Swap" is scratch space for an
 operating system, which allows the system to use disk storage as
 "virtual memory".
 .
 When the partition is already formatted you may choose to keep and
 use the existing data in the partition.  Partitions that will be used
 in this way are marked with "${KEEP}" in the main partitioning menu.
 .
 In general you will want to format the partition with a newly created
 file system.  NOTE: all data in the partition will be irreversibly
 deleted.  If you decide to format a partition that is already
 formatted, it will be marked with "${DESTROY}" in the main
 partitioning menu.  Otherwise it will be marked with "${FORMAT}".

Template: partman-auto/progress/title
Type: text
# :sl1:
_Description: Please wait...

Template: partman-auto/progress/info
Type: text
# :sl1:
_Description: Computing the new partitions...

Template: partman-auto/no_recipe
Type: error
# :sl2:
_Description: Failed to partition the selected disk
 This probably happened because the selected disk or free space is too
 small to be automatically partitioned.

Template: partman-auto/autopartitioning_failed
Type: error
# :sl2:
_Description: Failed to partition the selected disk
 This probably happened because there are too many (primary) partitions in
 the partition table.

Template: partman-auto/init_automatically_partition
Type: select
Choices-C: ${CHOICES}
Choices: ${DESCRIPTIONS}
# :sl1:
_Description: Partitioning method:
 The installer can guide you through partitioning a disk (using different
 standard schemes) or, if you prefer, you can do it manually. With guided
 partitioning you will still have a chance later to review and customise
 the results.
 .
 If you choose guided partitioning for an entire disk, you will next be
 asked which disk should be used.
Help: partman-auto/help

Template: partman-auto/disk
Type: string
Description: for internal use; can be preseeded
 Device to partition, in either devfs or non format

Template: partman-auto/method
Type: string
Description: for internal use; can be preseeded
 Method to use for partitioning

Template: partman-auto/cap-ram
Type: string
Default: false
Description: for internal use; can be preseeded
 Cap RAM size to specified size in MB, when calculating the swap
 partition size. By default this is disabled, so swap size is not capped,
 while preseeding to a value of 1024 (with partman-auto/cap-ram=1024) for
 example results in swap partitions to be capped at 1GB.

Template: partman-auto/automatically_partition
Type: select
Choices-C: ${CHOICES}
Choices: ${DESCRIPTIONS}
# :sl1:
_Description: Partitioning method:
 If you choose guided partitioning for an entire disk, you will next be
 asked which disk should be used.

Template: partman-auto/choose_recipe
Type: select
Choices-C: ${CHOICES}
Choices: ${DESCRIPTIONS}
# :sl1:
#flag:comment:2
# "Selected for partitioning" can be either an entire disk
# of "the largest continuous free space" on an existing disk
# TRANSLATORS, please take care to choose something appropriate for both
#
# It is followed by a variable giving the chosen disk, hence the colon
# at the end of the sentence. Please keep it.
#flag:translate!:3
_Description: Partitioning scheme:
 Selected for partitioning:
 .
 ${TARGET}
 .
 The disk can be partitioned using one of several different schemes.
 If you are unsure, choose the first one.
Help: partman-auto/help

Template: partman-auto/unusable_space
Type: error
# :sl2:
_Description: Unusable free space
 Partitioning failed because the chosen free space may not be used.
 There are probably too many (primary) partitions in the partition table.

Template: partman-auto/expert_recipe_file
Type: string
# Not translatable, this is for use by custom distributions that want
# to force the use of their own recipe.
Description: for internal use; can be preseeded
 File to load for expert recipe

Template: partman-auto/expert_recipe
Type: string
Description: for internal use; can be preseeded
 Expert recipe content

Template: partman-auto/text/automatically_partition
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Guided partitioning

Template: partman-auto/text/use_biggest_free
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Guided - use the largest continuous free space

Template: partman-auto/text/use_device
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Guided - use entire disk

Template: partman-auto/select_disk
Type: select
Choices-C: ${CHOICES}
Choices: ${DESCRIPTIONS}
# :sl1:
_Description: Select disk to partition:
 Note that all data on the disk you select will be erased, but not before
 you have confirmed that you really want to make the changes.

Template: partman-auto/text/custom_partitioning
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
# This is a method for partitioning - as in guided (automatic) versus manual
_Description: Manual

Template: partman-auto/text/auto_free_space
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Automatically partition the free space

Template: partman-auto/text/atomic_scheme
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: All files in one partition (recommended for new users)

Template: partman-auto/text/home_scheme
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Separate /home partition

Template: partman-auto/text/multi_scheme
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Separate /home, /var, and /tmp partitions

Template: partman-auto/text/small_disk
Type: text
# :sl2:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Small-disk (< 10GB) partitioning scheme

Template: partman-auto/text/server_scheme
Type: text
# :sl1:
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
_Description: Separate /var and /srv, swap < 1GB (for servers)
